import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({ comment, likeComment, dislikeComment }) => {
  const {
    id,
    body,
    user,
    likeCount,
    dislikeCount,
    createdAt
  } = comment;
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
      </CommentUI.Content>
      <CommentUI.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.objectOf(PropTypes.any).isRequired,
  dislikeComment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Comment;
