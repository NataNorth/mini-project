import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const likeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      isLike: true,
      isDislike: false,
      isSwitch: false
    }
  });
  return response.json();
};

export const dislikeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      isDislike: true,
      isLike: false,
      isSwitch: false
    }
  });
  return response.json();
};
