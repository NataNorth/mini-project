const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = app => {
  app.use(createProxyMiddleware('/ws', {target: 'http://127.0.0.1:8080', ws: true}))
}
