package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {
        var reaction = commentReactionsRepository.getCommentReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());
        if (reaction.isPresent()) {
            var react = reaction.get();
            Boolean isSwitch = Boolean.FALSE;
            if (react.getIsLike() == commentReactionDto.getIsLike() &&
                    react.getIsDislike() == commentReactionDto.getIsDislike()) {
                commentReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else if (react.getIsLike() != commentReactionDto.getIsLike() &&
                    react.getIsDislike() != commentReactionDto.getIsDislike()) {
                commentReactionsRepository.deleteById(react.getId());
                isSwitch = Boolean.TRUE;
            }
            react.setIsLike(commentReactionDto.getIsLike());
            react.setIsDislike(commentReactionDto.getIsDislike());
            react.setIsSwitch(isSwitch);
            var result = commentReactionsRepository.save(react);
            return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
        } else {
            var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
            var result = commentReactionsRepository.save(commentReaction);
            return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
        }
    }
}
