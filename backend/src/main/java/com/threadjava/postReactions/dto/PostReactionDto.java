package com.threadjava.postReactions.dto;

import lombok.Data;
import org.springframework.boot.autoconfigure.BackgroundPreinitializer;

import java.util.UUID;

@Data
public class PostReactionDto {
    private UUID id;
    private Boolean isLike;
    private Boolean isDislike;
    private Boolean isSwitch;
}
